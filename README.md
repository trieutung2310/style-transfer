# Important resource:
	Download link: https://drive.google.com/file/d/0B4WPKvZwUjF8UzJZZ1BVazlsNW8/
	Unzip the file.
	In resources folder:
	Copy trained_model folder and  deeplab_resnet_init.ckpt file into the ./deeplab folder
	Copy trained network vgg19.npy into the ./deep-photo-styletransfer-tf/vgg19 folder
	

# Install environment:
	pip install -r requirements.txt
	pip install tensorflow-gpu==1.2.0 
	pip install scikit-image

# Run the demo:
    
    In ./deeplab folder: segmentation task:
    
        * Generate full-information mask, manually-assigned mask and the texture image for the content image
        
        python inference.py  ./trained_model/model.ckpt-30000 --num-classes 19 --out_folder ./output/ --mode con --label "0" --file_name content_img1.png --folder_name ./data/content/
        
        * Generate full-information mask, manually-assigned mask and the texture image for the style image
        
        python inference.py  ./trained_model/model.ckpt-30000 --num-classes 19 --out_folder ./output/ --mode ff --label "0" --file_name img1.png --folder_name ./data/ff/
        
        ** Note:
            --mode ff: choose an image in folder ff
            --mode bio: choose an image in folder bio
            --mode con: choose an image in folder content
            "0" is the choosen label list, 0 represents road.
            If choosing label list manually: replace "0 1 2" into "label_code_1(space)label_code_2(space)...(space)label_code_n"

            Label code:
    
            0: road
            1: sidewalk
            2: building
            3: wall
            4: fence
            5: pole
            6: traffic_light
            7: traffic_sign
            8: vegetation
            9: terrain
            10: sky
            11: person
            12: raider
            13: car
            14: truck
            15: bus
            16: train
            17: motorcycle
            18: bicycle

    In ./deep-photo-styletransfer-tf: style transfer task
        
        python deep_photostyle.py --content_image_path ../deeplab_v2/output/con_new_img1.png --style_image_path ../deeplab_v2/output/ff_new_img1.png --content_seg_path ../deeplab_v2/output/con_new_mask1.png --style_seg_path ../deeplab_v2/output/ff_new_mask1.png --output_image ./style_transfer_result/result1.png --serial ./style_transfer_result/tmp_result1/ --max_iter 200 --style_option 2
        