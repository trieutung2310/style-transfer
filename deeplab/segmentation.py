from __future__ import print_function
import tensorflow as tf
import numpy as np
from PIL import Image
import cv2
import argparse

from deeplab_resnet import DeepLabResNetModel, ImageReader, decode_labels, prepare_label


IMG_MEAN = np.array((104.00698793,116.66876762,122.67891434), dtype=np.float32)

class Segmentation:
    def __init__(self, checkpoint, num_classes):
        self.checkpoint = checkpoint
        self.num_classes = num_classes

    def preprocessing(self, img):
        img -= IMG_MEAN
        return img


    def load_model(self):
            # Create network.
        self.data = tf.placeholder(tf.float32, shape=(None, None, None, 3))
        net = DeepLabResNetModel({'data': self.data}, is_training=False, num_classes=self.num_classes)

        # Which variables to load.
        restore_var = tf.global_variables()

        # Predictions.
        raw_output = net.layers['fc1_voc12']
        raw_output_up = tf.image.resize_bilinear(raw_output, tf.shape(self.data)[1:3,])
        raw_output_up = tf.argmax(raw_output_up, dimension=3)
        self.pred = tf.expand_dims(raw_output_up, dim=3)

        
        # Set up TF session and initialize variables. 
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        init = tf.global_variables_initializer()
        
        self.sess.run(init)
        
        # Load weights.
        loader = tf.train.Saver(var_list=restore_var)
        loader.restore(self.sess, self.checkpoint)

    def inference(self, img_arr):
        mask = self.sess.run(self.pred, {self.data: img_arr})
        return mask


if __name__=='__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("input", type=str, help="input image")
    parser.add_argument("model", type=str, help="trained model")
    parser.add_argument("--num-classes", type=int, help="Num of categories")
    parser.add_argument("--output", type=str, help="Names of output file", default="output.png")

    args = parser.parse_args()
    print (args)

    seg = Segmentation(args.model, args.num_classes)
    seg.load_model()
    #img_arr = np.random.randn(1, 320, 320, 3)
    
    img_arr = cv2.imread(args.input)
    mask = seg.inference(img_arr[None, :, :, :])
    msk = decode_labels(mask, num_images=1)
    img = mask.reshape(mask.shape[1:3]).astype(np.uint8)
    print (img.shape)
    im = Image.fromarray(img)
    im.save(args.output)