
"""Run DeepLab-ResNet on a given image.

This script computes a segmentation mask for a given image.
"""

from __future__ import print_function
from skimage import color
from skimage import io
import random
import argparse
from datetime import datetime
import os
import sys
import time

from PIL import Image

import tensorflow as tf
import numpy as np

from deeplab_resnet import DeepLabResNetModel, ImageReader, new_label_set, decode_labels, prepare_label

IMG_MEAN = np.array((104.00698793,116.66876762,122.67891434), dtype=np.float32)
    
NUM_CLASSES = 21
SAVE_DIR = './output/'
BIO_PATH = './data/bio/'
FF_PATH = './data/ff/'

def get_arguments():
    """Parse all the arguments provided from the CLI.
    
    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLabLFOV Network Inference.")
    parser.add_argument("--file_name", type = str, help = "name of the image file")
    parser.add_argument("model_weights", type=str,
                        help="Path to the file with model weights.")
    parser.add_argument("--num-classes", type=int, default=NUM_CLASSES,
                        help="Number of classes to predict (including background).")
    parser.add_argument("--out_folder", type=str, default='./data/',
                        help="Path to predicted mask.")
    parser.add_argument("--mode", type=str,
                        help="choose file from bio or ff (style image) or content image")
    parser.add_argument("--label", type=str, default="1 3",
                        help="choose the list of specific labels")
    parser.add_argument("--folder_name", type = str, help = "the image path")

    return parser.parse_args()

def load(saver, sess, ckpt_path):
    '''Load trained weights.
    
    Args:
      saver: TensorFlow saver object.
      sess: TensorFlow session.
      ckpt_path: path to checkpoint file with parameters.
    ''' 
    saver.restore(sess, ckpt_path)
    print("Restored model parameters from {}".format(ckpt_path))

def main():
    """Create the model and start the evaluation process."""
    args = get_arguments()
    try:
        os.makedirs(args.out_folder)
    except OSError:
        if not os.path.isdir(args.out_folder):
             raise    
    mode = args.mode
    out_file_ori = args.out_folder + mode + '_ori_mask1.png'
    out_file_new = args.out_folder + mode + '_new_mask1.png'
    new_img = args.out_folder + mode + '_new_img1.png'
    ori_img = args.out_folder + mode + '_img.png'
    img_path = args.folder_name + args.file_name
    print(img_path)

    img_ori = Image.open(img_path)
    # Prepare image.
    img = tf.image.decode_png(tf.read_file(img_path), channels=3)
    shape = tf.shape(img)
    img = tf.image.resize_images(img, [((720*shape[0])/shape[1]), 720])
    img1 = img
    # Convert RGB to BGR.
    img_r, img_g, img_b = tf.split(axis=2, num_or_size_splits=3, value=img)
    img = tf.cast(tf.concat(axis=2, values=[img_b, img_g, img_r]), dtype=tf.float32)
    # Extract mean.
    img -= IMG_MEAN 
    
    # Create network.
    net = DeepLabResNetModel({'data': tf.expand_dims(img, dim=0)}, is_training=False, num_classes=args.num_classes)

    # Which variables to load.
    restore_var = tf.global_variables()

    # Predictions.
    raw_output = net.layers['fc1_voc12']
    raw_output_up = tf.image.resize_bilinear(raw_output, tf.shape(img)[0:2,])
    raw_output_up = tf.argmax(raw_output_up, dimension=3)
    pred = tf.expand_dims(raw_output_up, dim=3)

    
    # Set up TF session and initialize variables. 
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    init = tf.global_variables_initializer()
    
    sess.run(init)
    
    # Load weights.
    loader = tf.train.Saver(var_list=restore_var)
    load(loader, sess, args.model_weights)
    
    # Perform inference.
    preds = sess.run(pred)

    msk = decode_labels(preds, num_classes=args.num_classes)
    # print(msk[0])
    # print(type(msk))
    im = Image.fromarray(msk[0])
   
    # print(type(im))
    im.save(out_file_ori)

    new_label = new_label_set(list_of_label=((args.label).split()))
    msk = decode_labels(preds, num_classes=args.num_classes,label_colour_set = new_label)

    im = Image.fromarray(msk[0])
    print(msk[0].shape)
    height = msk[0].shape[0]
    width = msk[0].shape[1]
    print(height)
    print(width)
    im_res = img_ori.resize((width,height),Image.BILINEAR)
    im_res1 = np.asarray(im_res)
    im_res1 = im_res1[:,:,0:3]
    print(im_res1.shape)
    im_new = np.zeros((height,width,3))
    print(im_new.shape)
    for i in range(0,height):
       for j in range(0,width):
           if np.all(msk[0][i,j,:,] == 0):
               im_new[i,j,:] = (0,0,0)
           else:
               im_new[i,j,:] = im_res1[i,j,:]
    im_new1 = Image.fromarray(im_new.astype('uint8'))
    im_new1.save(new_img)
    im.save(out_file_new)
    im_res.save(ori_img)

    print('The output files has been saved.')

    #im2 = 
if __name__ == '__main__':
    main()
