1. Important resource:
	Download link: https://drive.google.com/file/d/0B4WPKvZwUjF8UzJZZ1BVazlsNW8/

	-- Unzip the file.

	-- In resources folder:

	--- Copy "trained_model" folder and  "deeplab_resnet_init.ckpt" file into "./deeplab_v2" folder

2. Install environment:
	pip install -r requirements.txt
	pip install tensorflow-gpu==1.2.0 
	pip install scikit-image

3. Run the demo:

	python inference.py  ./trained_model/model.ckpt-30000 --num-classes 19 --out_folder ./data/ --mode ff --label "0 1 2" --file_name img1.jpg --folder_name ./data/ff/

# Note:
# --mode ff: choose the image in folder ff
# --mode bio: choose the image in folder bio

# "0 1 2" is the choosen label list, 0 represents road; 1 represents sidewalk; and 2 represents building
# If choosing label list manually: replace "0 1 2" into "label_code_1(space)label_code_2(space)...(space)label_code_n"

# Label code:
# 0: road
# 1: sidewalk
# 2: building
# 3: wall
# 4: fence
# 5: pole
# 6: traffic_light
# 7: traffic_sign
# 8: vegetation
# 9: terrain
# 10: sky
# 11: person
# 12: raider
# 13: car
# 14: truck
# 15: bus
# 16: train
# 17: motorcycle
# 18: bicycle


